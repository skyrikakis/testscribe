package com.github.scribejava.apis.examples;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.*;
import com.github.scribejava.core.oauth.OAuth20Service;
import com.github.scribejava.core.services.Base64Encoder;

import java.util.Base64;

public class OAuth20ServiceHellenicImpl extends OAuth20Service {

    public OAuth20ServiceHellenicImpl(DefaultApi20 api, OAuthConfig config) {
        super(api, config);
    }

    @Override protected OAuthRequest createAccessTokenRequest(String code) {
        final OAuthRequest request = new OAuthRequest(Verb.GET, getApi().getAccessTokenEndpoint());
        final OAuthConfig config = getConfig();
        request.addQuerystringParameter("clientId", config.getApiKey());
        request.addQuerystringParameter("clientSecret", config.getApiSecret());
        request.addQuerystringParameter("clientToken", code);
        return request;
    }

    @Override public void signRequest(String accessToken, OAuthRequest request) {
        request.addHeader("Authorization", getAuthorizationHeader(accessToken));
    }

    private String getAuthorizationHeader(String accessToken) {
        String basicAuth = getConfig().getApiKey() + ':' + getConfig().getApiSecret();
        basicAuth = Base64.getEncoder().encodeToString(basicAuth.getBytes());
        final StringBuilder sb = new StringBuilder();
        sb.append("Basic ");
        sb.append(basicAuth);
        sb.append(", jwt ");
        sb.append(Base64.getEncoder().encodeToString(accessToken.getBytes()));
        return sb.toString();
    }
}
