package com.github.scribejava.apis.examples.fidor;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

public class OAuth20FidorServiceImpl extends OAuth20Service {

    private static final String CALLBACK_URL = System.getenv("CALLBACK_URL");

    public OAuth20FidorServiceImpl(DefaultApi20 api, OAuthConfig config) {
        super(api, config);
    }

    @Override
    protected OAuthRequest createAccessTokenRequest(String clientToken) {

        final OAuthRequest request = new OAuthRequest(Verb.POST, getApi().getAccessTokenEndpoint());
        final OAuthConfig config = getConfig();
        request.addBodyParameter("client_id", config.getApiKey());
        request.addBodyParameter("redirect_uri",CALLBACK_URL);
        request.addBodyParameter("code", clientToken);
        request.addBodyParameter("client_secret", config.getApiSecret());
        request.addBodyParameter("grant_type", "authorization_code");
        return request;
    }

    @Override
    public void signRequest(String accessToken, OAuthRequest request) {
        super.signRequest(accessToken, request);
        request.addHeader("accept","application/vnd.fidor.de; version=1,text/json");
        request.addHeader("content-type","application/json");
    }
}
