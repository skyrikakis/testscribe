package com.github.scribejava.apis.examples.fidor;

public class FidorConstants {

    public static final String SANDBOX_API_ENDPOINT = "https://aps.fidor.de";
    public static final String SANDBOX_ACCESS_TOKEN_ENDPOINT = "https://aps.fidor.de/oauth/token";
    public static final String SANDBOX_AUTHORIZATION_BASE_URL = "https://aps.fidor.de/oauth/authorize";

    private FidorConstants() {
    }

}
