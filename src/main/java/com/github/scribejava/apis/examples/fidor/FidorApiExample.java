package com.github.scribejava.apis.examples.fidor;

import com.github.scribejava.core.builder.ServiceBuilder;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.oauth.OAuth20Service;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class FidorApiExample {

    private static final String CLIENT_ID = System.getenv("CLIENT_ID");
    private static final String CLIENT_SECRET = System.getenv("CLIENT_SECRET");
    private static final String SECRET_STATE = "secret" + new Random().nextInt(999_999);
    private static final String CALLBACK_URL = System.getenv("CALLBACK_URL");

    private static final Logger LOGGER = Logger.getLogger(FidorApiExample.class.toString());

    private FidorApiExample() {
    }

    public static void main(String... args) throws InterruptedException, ExecutionException, IOException, URISyntaxException {

        final OAuth20Service service = new ServiceBuilder(CLIENT_ID)
                .apiSecret(CLIENT_SECRET)
                .state(SECRET_STATE)
                .callback(CALLBACK_URL)
                .build(FidorApi20.instance());

        LOGGER.info("Retrieving authorization url...");

        final String authorizationUrl = service.getAuthorizationUrl(new HashMap<>());

        LOGGER.info("Retrieved authorization url "+authorizationUrl);

        try(Scanner scanner = new Scanner(System.in)) {

            System.out.print("Give the authorization code received > ");

            final String authorizationCode = scanner.next();

            LOGGER.info("Received token " + authorizationCode);


            System.out.print("Give the secret state > ");
            final String secretState = scanner.next();

            if(!secretState.equals(SECRET_STATE)) {

                LOGGER.info("Wrong secret state aborting program");
                return;
            }

            OAuth2AccessToken accessToken = service.getAccessToken(authorizationCode);

            LOGGER.info("Received the access token "+accessToken.getAccessToken()+" and the refresh token "+accessToken.getRefreshToken());

            UserService userService = new UserService(service);
            String accountInfo = userService.accountInfo(accessToken.getAccessToken());

            LOGGER.info("Received account info");

            System.out.println(accountInfo);
        }
    }

}
