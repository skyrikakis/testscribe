package com.github.scribejava.apis.examples;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.extractors.TokenExtractor;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.model.ParameterList;
import com.github.scribejava.core.oauth.OAuth20Service;

import java.util.Map;

public class HellenicBankApi20 extends DefaultApi20 {

    protected HellenicBankApi20() {

    }

    public static HellenicBankApi20 instance() {
        return HellenicBankApi20.InstanceHolder.INSTANCE;
    }

    @Override public String getAccessTokenEndpoint() {
        return "https://sandbox-oauth.hellenicbank.com/oauth/token";
    }

    @Override protected String getAuthorizationBaseUrl() {
        return "https://sandbox-oauth.hellenicbank.com/oauth/authorize";
    }

    @Override public String getAuthorizationUrl(OAuthConfig config, Map<String, String> additionalParams) {
        ParameterList parameters = new ParameterList(additionalParams);
        parameters.add("clientId", config.getApiKey());
        String callback = config.getCallback();
        if (callback != null) {
            parameters.add("redirectUri", callback);
        }

        String scope = config.getScope();
        if (scope != null) {
            parameters.add("scope", scope);
        }

        String state = config.getState();
        if (state != null) {
            parameters.add("state", state);
        }

        return parameters.appendTo(this.getAuthorizationBaseUrl());
    }

    @Override
    public OAuth20Service createService(OAuthConfig config) {
        return new OAuth20ServiceHellenicImpl(this, config);
    }

    @Override public TokenExtractor<OAuth2AccessToken> getAccessTokenExtractor() {
        return OAuth2AccessTokenHellenicBankExtractor.instance();
    }

    public String getRevokeTokenEndpoint() {
        return "https://sandbox-oauth.hellenicbank.com/oauth/token";
    }

    private static class InstanceHolder {
        private static final HellenicBankApi20 INSTANCE = new HellenicBankApi20();

        private InstanceHolder() {
        }
    }
}
