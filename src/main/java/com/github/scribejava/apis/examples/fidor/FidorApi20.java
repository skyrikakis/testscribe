package com.github.scribejava.apis.examples.fidor;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.model.OAuthConfig;
import com.github.scribejava.core.oauth.OAuth20Service;

public class FidorApi20 extends DefaultApi20 {

    protected FidorApi20() {
    }

    public static FidorApi20 instance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public OAuth20Service createService(OAuthConfig config) {
        return new OAuth20FidorServiceImpl(this,config);
    }

    @Override
    public String getAccessTokenEndpoint() {
        return FidorConstants.SANDBOX_ACCESS_TOKEN_ENDPOINT;
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return FidorConstants.SANDBOX_AUTHORIZATION_BASE_URL;
    }

    private static class InstanceHolder {
        private static final FidorApi20 INSTANCE = new FidorApi20();

        private InstanceHolder() {
        }
    }

}
