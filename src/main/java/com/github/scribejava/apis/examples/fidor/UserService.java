package com.github.scribejava.apis.examples.fidor;

import com.github.scribejava.core.model.OAuthRequest;
import com.github.scribejava.core.model.Response;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth.OAuth20Service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutionException;
import java.util.logging.Logger;

public class UserService {

    private OAuth20Service oAuth20Service;

    private static final Logger LOGGER = Logger.getLogger(UserService.class.toString());

    public UserService(OAuth20Service oAuth20Service) {
        this.oAuth20Service = oAuth20Service;
    }

    public String accountInfo(String token) throws IOException, ExecutionException, InterruptedException, URISyntaxException {

        final OAuthRequest request = new OAuthRequest(Verb.GET, getUserUri(token).toString());
        oAuth20Service.signRequest(token, request);
        final Response response = oAuth20Service.execute(request);
        return response.getBody();
    }

    private URI getUserUri(String token) throws URISyntaxException {
        return new URI(FidorConstants.SANDBOX_API_ENDPOINT + "/users/current?access_token=" + token);
    }

}
